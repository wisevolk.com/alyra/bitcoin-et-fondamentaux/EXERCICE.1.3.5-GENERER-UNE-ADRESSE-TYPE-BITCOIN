const crypto = require("crypto");
const SHA256 = require("crypto-js/sha256");
const RIPEM160 = require("crypto-js/ripemd160");
const bs58 = require("bs58");

const PREFIX = "0x00";
const CURVE = "secp256k1";
//TODO Generation d'une clé openssl via le bash.
// const { exec } = require("child_process");
// exec("openssl XXXXXX ", (error, stdout, stderr) => {
//     if (error) {
//         console.log(`error: ${error.message}`);
//         return;
//     }
//     if (stderr) {
//         console.log(`stderr: ${stderr}`);
//         return;
//     }
//     console.log(`stdout: ${stdout}`);
// });
/*Création d'une clé alétoire basé sur les caractères ASCII du code 33 à 127
 * @param length : longeur de la clé en nombre de caractères.
 */


function createKey() {
    const ecdh = crypto.createECDH(CURVE);
    ecdh.generateKeys();

    console.log("****************************  Random secp256k1 Public Key  ****************************");
    const key = ecdh.getPublicKey("hex");
    console.log("Random secp251k Public key : "+key);
    // const uncompressedKey = crypto.ECDH.convertKey(key,
    //     'secp256k1',
    //     'hex',
    //     'hex');
    // console.log(uncompressedKey);
}

function createRandomKey(_length) {
    console.log("****************************    Weak Random Public Key    ****************************\n");
    let compt = 0;
    let key = "";
    while ( compt <= _length ) {
        compt++;
        // on part sur la table de codage ASCII du caractère 33 au caractère 127
        key += String.fromCharCode(Math.round(Math.random() * (127 - 33)) + 33);
    }
    console.log("Random Key : " + key);
    return key;
}

function generateBitcoinKey(privateKey) {
    const hash160 = PREFIX + RIPEM160(SHA256(privateKey));
    const controlBytes = SHA256(SHA256(hash160));
    const bitecoinKeyHex = hash160 + controlBytes.toString().substring(0, 4);
    // console.log(bitecoinKeyHex);
    const bytes = Buffer.from(bitecoinKeyHex.substring(2), "hex");
    return bs58.encode(bytes);
}

console.clear();
const randomKey = createRandomKey(50);
console.log("Bitcoin Key : " + generateBitcoinKey(randomKey));
console.log("\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
console.log("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
const randomSecp251Key = createKey();
console.log("Bitcoin Key from secp256k1 : "+generateBitcoinKey(randomSecp251Key));

