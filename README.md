## Exercice 1.3.5 :Génèrer une adresse type bitcoin
Écrire un programme qui génère une adresse type bitcoin à partir d’une clé publique

* Choisir la clé publique aléatoirement

* Calculer le hash SHA 256 puis RIPEMD160 (voir librairies dans le cours), on appelle ce résultat hash160 

* Ajouter l’identifiant (0x00) au début, et le contrôle à la fin (4 premiers octets du sha256(sha256(0x00 + hash160)) )

* Convertir le nombre en base 58

NB: Les opérations doivent se faire sur la représentation binaire des nombres (bytes, buffer...)

[Discutons en ensemble sur le forum](https://forum.alyra.fr/t/exercice-1-3-5-generer-une-adresse-type-bitcoin/82)

Optionnel: 
Pour que votre outil soit complet, vous pouvez calculer la clé publique à partir de la clé privée
* [Générer la clé privée aléatoirement Se référer à la fiche](https://ecole.alyra.fr/mod/page/view.php?id=59)

* Calculer la clé publique EDSA Secp256k1 

* Représenter la clé publique sour la forme 0x04 X(sur 32bits) Y(sur 32 bits)

à*  partir de là appliquer les étapes précédentes (hash160, ajouter entête et clé, conversion en base58)
